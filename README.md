# MoneyRates

Money Rates is a test in Swift

## Installation

Download branch master and open in Xcode



## Include
* Xcode 13
* Swift 5
* MVVM
* URLSesion
* Codable
* UIKit
* Extensions
* Captures Screen


## License
[MIT](https://choosealicense.com/licenses/mit/)
